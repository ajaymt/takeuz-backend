
var _ = require('lodash');
var net = require('net');
var zmq = require('zmq');
var seaport = require('seaport');
var db = require('./lib/db');
var Lockstep = require('./lib/zmq-lockstep');

var ports = seaport.connect('localhost', 9090);
var sock = new Lockstep(zmq.socket('req'));
var workers = ports.query('zmq-workers');

for (var i in workers)
  sock.sock.connect('tcp://127.0.0.1:' + workers[i].port.toString());

ports.on('register', function (s) {
  if (s.role === 'zmq-worker')
    sock.sock.connect('tcp://127.0.0.1:' + s.port.toString());
});

var beacons = {};

net.createServer(function (s) {
  s.on('data', function (d) {
    var data = d.toString().split('&');
    var dv = {};
    var status = 200;

    for (var i in data) {
      var nvp = data[i].split('=');
      dv[nvp[0]] = nvp[1];
    }

    dv.key = dv.dId;
    dv.imei = dv.IMEI;
    dv.sim = dv.SimNo;

    if (beacons[s]) {
      if (! dv.feed) status = 705;

      s.write('rc=' + status.toString() + '\n');

      if (status !== 200) return;

      var loc = dv.feed.split(',').slice(0, 2);

      db.updateLocation(beacons[s], loc, function (err, result) {
        console.log(dv, result);
      });

      return;
    }

    if (! dv.imei) status = 701;
    else if (! dv.key) status = 703;
    else if (! dv.sim) status = 702;

    if (dv.reg === '1' && dv.imei && dv.sim) {
      status = 250;

      db.makeKey(dv.imei, dv.sim, function (err, flags, key) {
        if (flags.keyExists) status = 706;
        else if (! flags.imeiExists) status = 800;

        s.write('rc=' + status.toString() + '\n');

        if (status === 250 && key) s.write(key + '\n');
      });

      return;
    }

    if (status !== 200) {
      s.write('rc=' + status.toString() + '\n');

      return;
    }

    db.checkDevice(dv, function (err, flags) {
      if (flags.imeiWrong) status = 800;
      else if (flags.keyWrong) status = 704;
      else if (flags.simWrong) status = 700;

      s.write('rc=' + status.toString() + '\n');

      if (status === 200 && ! _.some(flags)) beacons[s] = dv;
    });
  });

  s.on('end', function () {
    delete beacons[s];
  });
}).listen(ports.register('tcp-worker'));
