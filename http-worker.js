
var express = require('express');
var seaport = require('seaport');

var ports = seaport.connect('localhost', 9090);

var app = express();

var deviceHandler = require('./lib/device-handler');

app.use('/SHOWER/Channel', deviceHandler);

app.listen(ports.register('http-worker'));
