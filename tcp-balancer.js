
var net = require('net');
var seaport = require('seaport');
var tproxy = require('tproxy');

var ports = seaport.connect('localhost', 9090);

var i = 0;

net.createServer(function (sock) {
  var workers = ports.query('tcp-worker');

  tproxy(sock, workers[i]);

  i = (i + 1) % workers.length;
}).listen(3001);
