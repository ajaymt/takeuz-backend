
var httpProxy = require('http-proxy');
var http = require('http');
var seaport = require('seaport');

var proxy = httpProxy.createServer({});

var ss = seaport.createServer();
ss.listen(9090);

var i = 0;

http.createServer(function (req, res) {
  var workers = ss.query('http-worker');

  if (! workers[i]) {
    res.writeHead(503);
    res.end('service unavailable');

    return;
  }

  proxy.web(req, res, { target: workers[i] });

  i = (i + 1) % workers.length;
}).listen(3000);

proxy.on('error', function (err, req, res) {
  res.end('internal error. try refreshing.');
});
