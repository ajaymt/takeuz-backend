
# takeuz-backend
takeuz's node.js backend.

## Setting up
```sh
$ npm install
```

## Running
Use `start.py`:
```sh
$ start.py [k | kill] [<http-workers> <tcp-workers> <zmq-workers>]
```

`k` or `kill` will kill all `node` processes.

`http/tcp/zmq-workers` are the number of http/tcp/zmq worker processes to start. Defaults to 1 each, unless `k` is also used.

Example usage:
```sh
$ start.py 2 2 1 # will start 2 http workers, 2 tcp workers & 1 zmq worker
$ start.py k 1 2 3 # will kill all 'node' processes and
                   # start 1 http worker, 2 tcp workers & 3 zmq workers
$ start.py k # will kill all node processes
```

## Components
- `http-balancer.js`: http load balancer
- `http-worker.js`: single http worker
- `tcp-balancer.js`: tcp load balancer
- `tcp-worker.js`: single tcp worker
- `zmq-worker.js`: single zmq worker
- `lib/device-handler`: express sub-app to handle http requests from beacons
- `lib/db.js`: bunch of functions that perform database queries
- `lib/zmq-lockstep.js`: wrapper around zmq req-rep sockets that provides proper req-rep async behaviour
- `lib/utils.js`: utility functions
