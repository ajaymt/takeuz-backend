
/* global require */

var zmq = require('zmq');
var seaport = require('seaport');
var Lockstep = require('./lib/zmq-lockstep');

var ports = seaport.connect('localhost', 9090);
var port = ports.register('zmq-worker');
var sock = new Lockstep(zmq.socket('rep'));

sock.sock.bind('tcp://127.0.0.1:' + port.toString());

sock.onMessage(function (msg) {
  console.log('data: ' + msg.body);

  sock.reply(msg, 'reply');
});
