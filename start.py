#!/usr/bin/env python

import sys
from os import system
from minopt import minopt

args = minopt(sys.argv[1:])['_']

if len(args) > 0:
    if args[0] == 'k' or args[0] == 'kill':
        system('killall node')

    if len(args) < 4:
        exit()

args = [int(n) for n in args[-3:]] or [1, 1, 1]
names = ['http', 'tcp', 'zmq']

system('node ./http-balancer.js &')
system('node ./tcp-balancer.js &')

for i, n in enumerate(args):
    for _ in range(n):
        system('node ' + './' + names[i] + '-worker.js &')
