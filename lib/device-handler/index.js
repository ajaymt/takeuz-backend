
var _ = require('lodash');
var express = require('express');
var zmq = require('zmq');
var seaport = require('seaport');
var db = require('../db');
var Lockstep = require('../zmq-lockstep');
var utils = require('../utils');

var ports = seaport.connect('localhost', 9090);
var sock = new Lockstep(zmq.socket('req'));
var workers = ports.query('zmq-worker');

for (var i in workers)
  sock.sock.connect('tcp://127.0.0.1:' + workers[i].port.toString());

ports.on('register', function (s) {
  if (s.role === 'zmq-worker')
    sock.sock.connect('tcp://127.0.0.1:' + s.port.toString());
});

// HTTP response codes:
//   IMEI not registered: 800
//   simNo not match: 700
//   perfect: 200
//   IMEI empty: 701
//   simNo empty: 702
//   deviceId empty: 703
//   deviceId not correct: 704
//   feed empty: 705
//   status is not matched: 706
//   register (reg = 1): 250

var app = module.exports = express();

app.get('/', function (req, res) {
  var imei = req.query.IMEI;
  var sim = req.query.SimNo;
  var id = req.query.dId;
  var reg = req.query.reg;
  var feed = req.query.feed;
  var dv = {
    imei: imei,
    sim: sim,
    key: id
  };

  var status = handleRequestParams(req.query);

  if (reg === '1' && imei && sim) {
    res.status(250);

    db.makeKey(imei, sim, function (err, flags, key) {
      res.set('Content-Type', 'text/plain');

      if (flags.keyExists || (! flags.imeiExists)) {
        if (flags.keyExists) res.status(706);
        else res.status(800);

        res.end();
        return;
      }

      if (flags.otherSimExists); // device has changed sims

      res.end(key);
    });

    return;
  }

  if (status !== 200) {
    res.status(status);
    res.end();

    return;
  }

  var loc = feed.split(',').slice(1, 3);
  loc = _.map(loc, utils.convertCoords);

  db.checkDevice(dv, function (err, flags) {
    if (flags.imeiWrong) status = 800;
    else if (flags.keyWrong) status = 704;
    else if (flags.simWrong) status = 700;

    res.status(status);
    res.end();

    if (status !== 200) return;

    db.updateLocation(dv, loc, function (err, result) {
      sock.send(JSON.stringify(dv), console.log);
    });
  });
});

function handleRequestParams (params) {
  var imei = params.IMEI;
  var id = params.dId;
  var sim = params.SimNo;
  var feed = params.feed;

  if (! imei) return 701;
  if (! sim) return 702;
  if (! id) return 703;
  if (! feed) return 705;

  return 200;
}
