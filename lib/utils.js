
module.exports.convertCoords = function (n) {
  n /= 100;
  var decimals = ((n - Math.floor(n)) * 100) / 60;
  n = Math.floor(n) + decimals;

  return Math.round(n * 1e6) / 1e6;
}
