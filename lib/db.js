
/* global require, module */

var _ = require('lodash');
var mysql = require('mysql');
var uid = require('uid');

var conn = mysql.createConnection({
  host: '128.199.113.235',
  database: 'test',
  user: 'ram',
  password: 'bsvr412'
});

module.exports.query = function (table, constraints, type, cb) {
  var sql = 'SELECT * FROM ' + table + ' WHERE '
  type = type ? 'AND' : 'OR';

  for (var k in constraints) {
    var val = constraints[k];

    if (typeof val === 'string') val = '"' + val + '"';

    sql += k + '=' + val + ' ' + type + ' '
  }

  sql = sql.substring(0, sql.length - (type.length + 2));

  conn.query(sql, cb);
}

module.exports.makeKey = function (imei, sim, cb) {
  var key = uid(32);

  var select = {
    sql: 'SELECT * FROM TTU_DEVICE WHERE DV_IMEI=? OR DV_SIM=?',
    values: [imei, sim]
  };

  var updateSQL = 'UPDATE TTU_DEVICE SET DV_KEY=?, DV_SEND_STATUS=1 '
                + 'WHERE DV_IMEI=? AND DV_SIM=?';

  var update = {
    sql: updateSQL,
    values: [key, imei, sim]
  };

  conn.query(select, function (err, res) {
    var imeis = _.where(res, { DV_IMEI: imei });
    var all = _.where(imeis, { DV_SIM: sim });
    var otherSim = _.reject(imeis, 'DV_SIM', sim);

    var flags = {
      keyExists: _.filter(all, 'DV_KEY').length > 0,
      imeiExists: imeis.length > 0,
      otherSimExists: (otherSim.length > 0 ?
                       otherSim[0].DV_KEY : false)
    };

    if (flags.keyExists || ! flags.imeiExists) {
      cb(err, flags, null);

      return;
    }

    conn.query(update, function (err, res) {
      cb(err, flags, key);
    });
  });
}

module.exports.checkDevice = function (dv, cb) {
  var imei = dv.imei;
  var sim = dv.sim;
  var key = dv.key;

  var constraints = {
    DV_IMEI: imei,
    DV_SIM: sim,
    DV_KEY: key
  };

  this.query('TTU_DEVICE', constraints, false, function (err, result) {
    var perfect = _.where(result, constraints);
    var flags = {};

    if (perfect.length > 0) {
      cb(err, flags);

      return;
    }

    var withoutIMEI = _.where(result, { DV_KEY: key, DV_SIM: sim });
    var withoutKey = _.where(result, { DV_IMEI: imei, DV_SIM: sim });
    var withoutSIM = _.where(result, { DV_IMEI: imei, DV_KEY: key });

    flags = {
      imeiWrong: withoutIMEI.length > 0,
      simWrong: withoutSIM.length > 0,
      keyWrong: withoutKey.length > 0
    };

    cb(err, flags);
  });
}

module.exports.updateLocation = function (dv, loc, cb) {
  var sql = 'UPDATE TTU_DEVICE SET DV_LATITUDE=?, DV_LONGITUDE=? WHERE DV_KEY=?'
          + 'AND DV_IMEI=? AND DV_SIM=?';

  var query = {
    sql: sql,
    values: [loc[0], loc[1], dv.key, dv.imei, dv.sim]
  };

  conn.query(query, cb);
}
