
var zmq = require('zmq');
var uid = require('uid');

function Lockstep (sock) {
  this.sock = sock;
  this.pending = {};
  this.sep = '::';

  var self = this;

  this.sock.on('message', function (msg) {
    msg = self.read(msg);

    if (! self.pending[msg.id]) return;

    self.pending[msg.id](msg.body);
    delete self.pending[msg.id];
  });
}

Lockstep.prototype.onMessage = function (cb) {
  var self = this;

  this.sock.on('message', function (msg) {
    cb(self.read(msg));
  });
}

Lockstep.prototype.send = function (msg, cb) {
  var msgid = uid(30);
  msg = {
    id: msgid,
    body: msg.toString()
  };

  this.pending[msg.id] = cb;
  this.sock.send(this.write(msg));
}

Lockstep.prototype.reply = function (msg, body) {
  msg = {
    id: msg.id,
    body: body.toString()
  };

  this.sock.send(this.write(msg));
}

Lockstep.prototype.read = function (msg) {
  var msgid = msg.toString().split(this.sep)[0];
  var msgbody = msg.toString().split(this.sep)
                .slice(1).join(this.sep);

  var obj = {
    body: msgbody
  };

  Object.defineProperty(obj, 'id', {
    value: msgid,
    writable: false
  });

  return obj;
}

Lockstep.prototype.write = function (msg) {
  return msg.id.toString() + this.sep + msg.body.toString();
}

module.exports = Lockstep;
